#from: https://stackoverflow.com/questions/3207219/how-do-i-list-all-files-of-a-directory
#TO DO: Use os.rmdir() to remove leftover empty directories https://www.dummies.com/programming/python/how-to-delete-a-file-in-python/
#TO DO: Remove any potential .srt or other subtitle files in the directory
#TO DO: Fix for TV shows, it currently sees tv shows as the same, they return the same title. Will need to compare against season and episode number.
#take title + season + episode for tv shows, if it cannot detect all of the three fields then it removes that entry
import os
import cv2
import PTN
import json
from difflib import SequenceMatcher #for checking string similarity

def manual_approval(loc):
	try:
		g = int(input("Approved? [1 = Yes; 2 = No] "))
	except:
		manual_approval(loc)
		
	else:
		if g == 1:
			print ("Success! " + array1[loc])
			try:
				print ("Would try to delete now...")
				#os.remove(array1[loc])

			except Exception as inst:
				print ("Encountered an unknown error handling " + array1[loc])
				print ("Error")


		elif g == 2:
			print ("Skipping...")

		else:
			print ("Sorry, I didn't understand that. Try again.")
			manual_approval(loc)


array1 = [os.path.join(r,file) for r,d,f in os.walk("F:\Recorded Movies") for file in f] #Put directory you want to clean in here

y = 0
for str in array1: #This is pretty yucky, but it works to remove unwanted file extensions at the moment
	if '.srt' in str:
		del array1[y]
		
	elif '.nfo' in str:
		del array1[y]

	elif '.png' in str:
		del array1[y]

	elif '.jpg' in str:
		del array1[y]

	elif '.jpeg' in str:
		del array1[y]

	elif '.exe' in str:
		del array1[y]

	elif '.txt' in str:
		del array1[y]

	elif '.pdf' in str:
		del array1[y]
		
	elif '.idx' in str:
		del array1[y]
		
	elif '.sub' in str:
		del array1[y]

	y += 1

del y

array2 = array1[:] #The colon tells it to directly copy rather than do a link

for x in range(0, len(array2)):
	array2[x].encode('utf-8').strip()
	print (array2[x])
	array2[x] = (json.dumps(PTN.parse(array2[x])))
	#print array2[x]
	array2[x] = (json.loads(array2[x])['title'])
	head, array2[x] = os.path.split(array2[x])

y = len(array2)
incrementCount = 0

for x in range(0, y):

	if array2[x]: #Removes null strings

		for i in range(x, y): #Set to x+1 so that it does not compare against the current file

			if x != i:
				if array2[i]: #Removes null strings
					similarity = SequenceMatcher(None, array2[x], array2[i])
					if similarity.ratio() >= 0.95: #Helps reduce false negatives, if the two strings if are 98% similar or greater then it catches them. You should be able to go lower than that, but I would rather be on the safe side, as some movies or tv shows have almost identical naming schemes for sequals

						try:
							with open(array1[x]) as capture1:
								print ("Comparing " + array1[x])
								capture1 = cv2.VideoCapture(array1[x]) #Open the video
								ret, frame = capture1.read() #Read the first frame
								resolution1 = frame.shape #Get resolution
								capture1.release()

						except Exception as inst:
							print ("Encountered an unknown error handling " + array1[x])
							print ("Error")
							resolution1 = (0, 0, 0)
							break

						try:
							with open(array1[i]) as capture2:
								print ("With      " + array1[i])
								capture2 = cv2.VideoCapture(array1[i]) #Open the video
								ret, frame = capture2.read() #Read the first frame
								resolution2 = frame.shape #Get resolution
								capture2.release()

						except Exception as inst:
							print ("Encountered an unknown error handling " + array1[i])
							print ("Error")
							resolution2 = (0, 0, 0)
							break

						if resolution1 > resolution2:
							print ("Would delete: ")
							print (array1[i])
							manual_approval(i)
							incrementCount += 1
							array1[i] = ''
							array2[i] = ''

						if resolution2 > resolution1:
							print ("Would delete: ")
							print (array1[i])
							manual_approval(i)
							incrementCount += 1
							array1[x] = ''
							array2[x] = ''

						if resolution1 == resolution2:
							try:
								size1 = os.path.getsize(array1[x])
								size2 = os.path.getsize(array1[i])

							except Exception as inst:
								print ("Encountered an unknown error handling " + array1[i])
								print ("Error")

							else:
								if size1 < size2:
									print ("Would delete: ")
									print (array1[i])
									manual_approval(i)
									incrementCount += 1
									array1[i] = ''
									array2[i] = ''

								if size1 > size2:
									print ("Would delete: ")
									print (array1[x])
									manual_approval(x)
									incrementCount += 1
									array1[x] = ''
									array2[x] = ''

								if size1 == size2: # In the incredibly unlikely event that both files are the same content with the same file size, it will delete the one with a shorter filename
									print ("Both equal, would delete: ")
									if len(array1[i]) < len(array1[x]):
										print (array1[x])
										manual_approval(x)

									if len(array1[i]) >= len(array1[x]):
										print (array1[i])
										manual_approval(i)
